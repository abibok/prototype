from django.db import models


class Person(models.Model):
    first_name = models.CharField(null=True, max_length=250, db_index=True)
    last_name = models.CharField(null=True, max_length=250, db_index=True)
    birthdate = models.DateField(null=True)
    email = models.EmailField(null=True)
    home_city = models.CharField(null=True, max_length=250)
    home_zip = models.CharField(null=True, max_length=250)
    home_address = models.CharField(null=True, max_length=250)
    phone = models.CharField(null=True, max_length=250)
    company_name = models.CharField(null=True, max_length=250)
    work_city = models.CharField(null=True, max_length=250)
    work_address = models.CharField(null=True, max_length=250)
    position = models.CharField(null=True, max_length=250)
    cv = models.TextField(null=True)
